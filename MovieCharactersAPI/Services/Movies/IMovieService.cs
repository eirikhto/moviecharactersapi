﻿using MovieCharactersAPI.Models;
using MovieCharactersAPI.Services;

namespace WebAPIWithEntityFramework.Services.Movies
{
    public interface IMovieService : ICrudService<Movie, int>
    {
        Task<object> GetAllCharactersInMovieAsync(int id);
        Task UpdateCharactersInMovie(int id, int[] characters);
    }
}
