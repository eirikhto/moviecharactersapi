﻿using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Models;
using WebAPIWithEntityFramework.Services.Movies;

namespace MovieCharactersAPI.Services.Movies
{
    public class MovieService : IMovieService
    {
        private readonly MovieCharactersContext _context;

        public MovieService(MovieCharactersContext context)
        {
            _context = context;
        }
        public async Task<Movie> AddAsync(Movie obj)
        {
            await _context.Movies.AddAsync(obj);
            await _context.SaveChangesAsync();
            return obj;
        }

        public async Task<IEnumerable<Movie>> GetAllAsync()
        {
            return await _context.Movies
                .Include(p => p.Characters)
                .Include(p => p.Franchise)
                .ToListAsync();
        }

        public async Task<object> GetAllCharactersInMovieAsync(int id)
        {
            return await _context.Characters
                .Where(c => c.Movies.Any(m => m.Id == id))
                .Include(c => c.Movies)
                .ToListAsync();
        }

        public async Task<Movie> GetByIdAsync(int id)
        {
            return await _context.Movies
                .Where(p => p.Id == id)
                .Include(p => p.Characters)
                .Include(p => p.Franchise)
                .FirstAsync();
        }

        public async Task<Movie> UpdateAsync(Movie obj)
        {
            _context.Entry(obj).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return obj;
        }

        public async Task<Movie> Delete(int id)
        {
            var movie = await _context.Movies.FindAsync(id);

            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();
            return null;
        }

        public async Task UpdateCharactersInMovie(int id, int[] characters)
        {
            var movie = await _context.Movies.Where(m => m.Id == id).Include(m => m.Characters).FirstOrDefaultAsync();
            var charactersEntity = new List<Character>();
            foreach (var character in characters)
            {
                charactersEntity.Add(await _context.Characters.FindAsync(character));
            }
            movie.Characters = charactersEntity;
            _context.Entry(movie).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
    }
}
