﻿using MovieCharactersAPI.Models;
using MovieCharactersAPI.Services;

namespace WebAPIWithEntityFramework.Services.Characters
{
    public interface ICharacterService : ICrudService<Character, int>
    {
    }
}
