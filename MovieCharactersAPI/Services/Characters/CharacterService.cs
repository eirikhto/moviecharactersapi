﻿using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Models;
using WebAPIWithEntityFramework.Services.Characters;

namespace MovieCharactersAPI.Services.Characters
{
    public class CharacterService : ICharacterService
    {
        private readonly MovieCharactersContext _context;

        public CharacterService(MovieCharactersContext context)
        {
            _context = context;
        }

        public async Task<Character> AddAsync(Character obj)
        {
            await _context.Characters.AddAsync(obj);
            await _context.SaveChangesAsync();
            return obj;
        }

        public async Task<IEnumerable<Character>> GetAllAsync()
        {
            return await _context.Characters
                .Include(p => p.Movies)
                .ToListAsync();
        }

        public async Task<Character> GetByIdAsync(int id)
        {
            return await _context.Characters
                .Where(p => p.Id == id)
                .Include(p => p.Movies)
                .FirstAsync();
        }

        public async Task<Character> UpdateAsync(Character obj)
        {
            _context.Entry(obj).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return obj;
        }
        public async Task<Character> Delete(int id)
        {
            var character = await _context.Characters.FindAsync(id);
            
            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();
            return null; 
        }

    }
}
