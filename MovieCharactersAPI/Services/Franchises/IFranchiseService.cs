﻿using MovieCharactersAPI.Models;
using MovieCharactersAPI.Services;

namespace WebAPIWithEntityFramework.Services.Franchises
{
    public interface IFranchiseService : ICrudService<Franchise, int>
    {
        Task<IEnumerable<Character>> GetAllCharactersInFranchiseAsync(int id);
        Task<IEnumerable<Movie>> GetAllMoviesInFranchiseAsync(int id);
        Task UpdateMoviesInFranchise(int id, int[] movies);
    }
}
