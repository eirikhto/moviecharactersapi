﻿using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Models;
using WebAPIWithEntityFramework.Services.Franchises;

namespace MovieCharactersAPI.Services.Franchises
{
    public class FranchiseService : IFranchiseService
    {
        private readonly MovieCharactersContext _context;

        public FranchiseService(MovieCharactersContext context)
        {
            _context = context;
        }

        public async Task<Franchise> AddAsync(Franchise obj)
        {
            await _context.Franchises.AddAsync(obj);
            await _context.SaveChangesAsync();
            return obj;
        }

        public async Task<IEnumerable<Franchise>> GetAllAsync()
        {
            return await _context.Franchises
                .Include(p => p.Movies)
                .ToListAsync();
        }

        public async Task<IEnumerable<Character>> GetAllCharactersInFranchiseAsync(int id)
        {
            return await _context.Characters
                .Where(c => c.Movies.Any(m => m.FranchiseId == id))
                .Include(p => p.Movies)
                .ToListAsync();
        }

        public async Task<IEnumerable<Movie>> GetAllMoviesInFranchiseAsync(int id)
        {
            return await _context.Movies
                .Where(m => m.FranchiseId == id)
                .Include(p => p.Characters)
                .Include(p => p.Franchise)
                .ToListAsync();
        }

        public async Task<Franchise> GetByIdAsync(int id)
        {
            return await _context.Franchises
                .Where(p => p.Id == id)
                .Include(p => p.Movies)
                .FirstAsync();
        }

        public async Task<Franchise> UpdateAsync(Franchise obj)
        {
            _context.Entry(obj).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return obj;
        }

        public async Task<Franchise> Delete(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);

            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();
            return null;
        }

        public async Task UpdateMoviesInFranchise(int id, int[] movies)
        {
            var franchise = await _context.Franchises.Where(f => f.Id == id).Include(f => f.Movies).FirstOrDefaultAsync();
            var moviesEntity = new List<Movie>();
            foreach(var movie in movies)
            {
                moviesEntity.Add(await _context.Movies.FindAsync(movie));
            }
            franchise.Movies = moviesEntity;
            _context.Entry(franchise).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
    }
}
