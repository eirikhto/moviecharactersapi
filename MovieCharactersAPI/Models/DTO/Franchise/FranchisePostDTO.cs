﻿using System.ComponentModel.DataAnnotations;

namespace MovieCharactersAPI.Models.DTO.Franchise
{
    public class FranchisePostDTO
    {
        public string Name { get; set; } = string.Empty;
        public string? Description { get; set; }
    }
}
