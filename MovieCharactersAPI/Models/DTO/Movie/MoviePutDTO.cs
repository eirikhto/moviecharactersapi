﻿namespace MovieCharactersAPI.Models.DTO.Movie
{
    public class MoviePutDTO
    {
        public int Id { get; set; }
        public string MovieTitle { get; set; } = string.Empty;
        public string? Genre { get; set; }
        public string Director { get; set; } = string.Empty;
        public string MoviePosterURL { get; set; } = string.Empty;
        public string TrailerURL { get; set; } = string.Empty;
    }
}
