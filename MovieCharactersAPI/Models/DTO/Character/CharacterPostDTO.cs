﻿using System.Reflection.Metadata.Ecma335;

namespace MovieCharactersAPI.Models.DTO.Character
{
    public class CharacterPostDTO
    {
        public string FullName { get; set; }
        public string? Alias { get; set; }
        public string? Gender { get; set; }
        public string PictureURL { get; set; }

    }
}
