﻿namespace MovieCharactersAPI.Models.DTO.Character
{
    public class CharacterPutDTO
    {
        public int Id { get; set; }
        public string FullName { get; set; } = string.Empty;
        public string? Alias { get; set; }
        public string? Gender { get; set; }
        public string PictureURL { get; set; } = string.Empty;
    }
}
