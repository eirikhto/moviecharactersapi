﻿using MessagePack.Formatters;
using System.ComponentModel.DataAnnotations;

namespace MovieCharactersAPI.Models.DTO.Character
{
    public class CharacterDTO
    {
        public int Id { get; set; }
        public string FullName { get; set; } = string.Empty;
        public string? Alias { get; set; }
        public string? Gender { get; set; }
        public string PictureURL { get; set; } = string.Empty;

        public List<int>? Movies { get; set; }
    }
}
