﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MovieCharactersAPI.Models
{
    [Table("Movie")]
    public class Movie
    {
        public Movie() 
        {
            Characters = new List<Character>();
        }
        public int Id { get; set; }
        [MaxLength(100)]
        public string MovieTitle { get; set; } = string.Empty;
        [MaxLength(100)]
        public string? Genre { get; set; }
        [MaxLength(50)]
        public string Director { get; set; } = string.Empty;
        public string MoviePosterURL { get; set; } = string.Empty;
        public string TrailerURL { get; set; } = string.Empty;

        //Relationships

        public int? FranchiseId { get; set; }
        public Franchise? Franchise { get; set; }

        public virtual ICollection<Character> Characters { get; set; } 

    }
}
