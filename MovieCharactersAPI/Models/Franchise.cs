﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MovieCharactersAPI.Models
{
    [Table("Franchise")]
    public class Franchise
    {
        public Franchise() 
        {
            Movies = new HashSet<Movie>();
        }
        public int Id { get; set; }
        [MaxLength(300)]
        public string Name { get; set; } = string.Empty;
        public string? Description { get; set; }

        //Realtionships

        public virtual ICollection<Movie> Movies { get; set; }

    }
}
