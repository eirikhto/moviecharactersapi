﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;

namespace MovieCharactersAPI.Models
{
    public class MovieCharactersContext : DbContext
    {
        public MovieCharactersContext()
        {
        }
        public MovieCharactersContext(DbContextOptions<MovieCharactersContext> options) : base(options)
        {
        }

        public DbSet<Character> Characters { get; set; }
        public DbSet<Movie> Movies { get;set; }
        public DbSet<Franchise> Franchises { get;set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Franchise>().HasData(
                new Franchise() { Id = 1 ,  Name = "Lord of the Rings", Description = "Best trilogy ever"});

            modelBuilder.Entity<Franchise>().HasData(
                new Franchise() { Id = 2, Name = "Disney Animated Classics", Description = "Animated Movies for Children" });

            modelBuilder.Entity<Movie>().HasData(
                new Movie()
                {
                    Id = 1,
                    MovieTitle = "The Fellowship of the Ring",
                    Genre = "Fantasy",
                    Director = "Peter Jackson",
                    MoviePosterURL = "https://www.themoviedb.org/t/p/original/6oom5QYQ2yQTMJIbnvbkBL9cHo6.jpg",
                    TrailerURL = "https://www.youtube.com/watch?v=_nZdmwHrcnw",
                    FranchiseId = 1
                });

            modelBuilder.Entity<Movie>().HasData(
                new Movie()
                {
                    Id = 2,
                    MovieTitle = "The Two Towers",
                    Genre = "Fantasy",
                    Director = "Peter Jackson",
                    MoviePosterURL = "https://www.themoviedb.org/t/p/original/5VTN0pR8gcqV3EPUHHfMGnJYN9L.jpg",
                    TrailerURL = "https://www.youtube.com/watch?v=nuTU5XcZTLA",
                    FranchiseId = 1
                });

            modelBuilder.Entity<Movie>().HasData(
                new Movie()
                {
                    Id = 3,
                    MovieTitle = "The Lion King",
                    Genre = "Animation",
                    Director = "Roger Allers",
                    MoviePosterURL = "https://images.theposterdb.com/prod/public/images/posters/optimized/movies/3280/03UBQg5JuDTIXa64mbQy6wfhIL27F9l45AM5KRGl.webp",
                    TrailerURL = "https://www.youtube.com/watch?v=lFzVJEksoDY",
                    FranchiseId = 1
                });

            modelBuilder.Entity<Character>().HasData(
                new Character()
                {
                    Id = 1,
                    FullName = "Meriadoc Brandybuck",
                    Alias = "Merry",
                    Gender = "Male",
                    PictureURL = "https://static.wikia.nocookie.net/lotr/images/d/d8/Merry1.jpg/revision/latest?cb=20080318214905"
                });

            modelBuilder.Entity<Character>().HasData(
                new Character()
                {
                    Id = 2,
                    FullName = "Gandalv",
                    Alias = "Mithrandir",
                    Gender = "Male",
                    PictureURL = "https://static.wikia.nocookie.net/lotr/images/e/e7/Gandalf_the_Grey.jpg/revision/latest/scale-to-width-down/700?cb=20121110131754"
                });

            modelBuilder.Entity<Character>().HasData(
                new Character()
                {
                    Id = 3,
                    FullName = "Théoden",
                    Gender = "Male",
                    PictureURL = "https://static.wikia.nocookie.net/lotr/images/1/13/King_Theoden_1.jpg/revision/latest?cb=20150313135224"
                });

            modelBuilder.Entity<Character>().HasData(
                new Character()
                {
                    Id = 4,
                    FullName = "Sméagol",
                    Alias = "Gollum",
                    Gender = "Male",
                    PictureURL = "https://static.wikia.nocookie.net/lotr/images/e/e1/Gollum_Render.png/revision/latest?cb=20141218075509"
                });

            modelBuilder.Entity<Character>().HasData(
                new Character()
                {
                    Id = 5,
                    FullName = "Peregrin Took",
                    Alias = "Pippin",
                    Gender = "Male",
                    PictureURL = "https://static.wikia.nocookie.net/lotr/images/0/0a/Pippinprintscreen.jpg/revision/latest?cb=20060310083048"
                });

            modelBuilder.Entity<Character>().HasData(
                new Character()
                {
                    Id = 6,
                    FullName = "Arwen",
                    Alias = "Evenstar",
                    Gender = "Female",
                    PictureURL = "https://static.wikia.nocookie.net/lotr/images/6/64/Arwen_-_The_Fellowship_Of_The_Ring.jpg/revision/latest?cb=20210625164207"
                });

            modelBuilder.Entity<Character>().HasData(
                new Character()
                {
                    Id = 7,
                    FullName = "Scar",
                    Alias = "Backstabbing MF",
                    Gender = "Male",
                    PictureURL = "https://static.wikia.nocookie.net/disney/images/6/66/Profile_-_Scar.jpeg/revision/latest/scale-to-width-down/1032?cb=20190312021344"
                });

            modelBuilder.Entity<Character>()
                .HasMany(c => c.Movies)
                .WithMany(m => m.Characters)
                .UsingEntity<Dictionary<string, object>>(
                    "CharacterMovie",
                    r => r.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                    l => l.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                    je =>
                    {
                        je.HasKey("CharacterId", "MovieId");
                        je.HasData(
                            new { CharacterId = 1, MovieId = 1 },
                            new { CharacterId = 2, MovieId = 1 },
                            new { CharacterId = 4, MovieId = 1 },
                            new { CharacterId = 5, MovieId = 1 },
                            new { CharacterId = 6, MovieId = 1 },
                            new { CharacterId = 1, MovieId = 2 },
                            new { CharacterId = 2, MovieId = 2 },
                            new { CharacterId = 3, MovieId = 2 },
                            new { CharacterId = 4, MovieId = 2 },
                            new { CharacterId = 5, MovieId = 2 },
                            new { CharacterId = 6, MovieId = 2 },
                            new { CharacterId = 7, MovieId = 3 });
                    });

        }
    }
}
