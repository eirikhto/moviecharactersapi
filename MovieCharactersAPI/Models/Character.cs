﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MovieCharactersAPI.Models
{
    [Table("Character")]
    public class Character
    {
        public Character() 
        {
            Movies = new HashSet<Movie>();
        }
        public int Id { get; set; }
        [MaxLength(50)]
        public string FullName { get; set; } = string.Empty;
        [MaxLength(100)]
        public string? Alias { get; set; }
        [MaxLength(20)]
        public string? Gender { get; set; }
        [MaxLength(500)]
        public string PictureURL { get; set; } = string.Empty;

        //Relationships
        public virtual ICollection<Movie> Movies { get; set; } = null!;


    }
}
