using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Services.Characters;
using MovieCharactersAPI.Services.Franchises;
using MovieCharactersAPI.Services.Movies;
using System.Reflection;
using WebAPIWithEntityFramework.Services.Characters;
using WebAPIWithEntityFramework.Services.Franchises;
using WebAPIWithEntityFramework.Services.Movies;

namespace WebAPIWithEntityFramework
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // Configuring generated XML docs for Swagger
            var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
            var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);


            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.

            builder.Services.AddControllers();
            builder.Services.AddDbContext<MovieCharactersContext>(
                opt => opt.UseSqlServer(builder.Configuration.GetConnectionString("default")));
            builder.Services.AddTransient<ICharacterService, CharacterService>();
            builder.Services.AddTransient<IFranchiseService, FranchiseService>();
            builder.Services.AddTransient<IMovieService, MovieService>();
            builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());




            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "Movie Character API",
                    Description = "API for a simple movie database",
                    Contact = new OpenApiContact
                    {
                        Name = "Eirik Torp",
                        Url = new Uri("https://gitlab.com/eirikhto")
                    },
                    License = new OpenApiLicense
                    {
                        Name = "MIT 2023",
                        Url = new Uri("https://opensource.org/licenses/MIT")
                    }
                });
                options.IncludeXmlComments(xmlPath);
            }
);

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();

            app.UseAuthorization();

            app.MapControllers();

            app.Run();
        }
    }
}