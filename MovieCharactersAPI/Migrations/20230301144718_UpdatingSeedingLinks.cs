﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MovieCharactersAPI.Migrations
{
    /// <inheritdoc />
    public partial class UpdatingSeedingLinks : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Character",
                keyColumn: "Id",
                keyValue: 1,
                column: "PictureURL",
                value: "https://static.wikia.nocookie.net/lotr/images/d/d8/Merry1.jpg/revision/latest?cb=20080318214905");

            migrationBuilder.UpdateData(
                table: "Character",
                keyColumn: "Id",
                keyValue: 2,
                column: "PictureURL",
                value: "https://static.wikia.nocookie.net/lotr/images/e/e7/Gandalf_the_Grey.jpg/revision/latest/scale-to-width-down/700?cb=20121110131754");

            migrationBuilder.UpdateData(
                table: "Character",
                keyColumn: "Id",
                keyValue: 3,
                column: "PictureURL",
                value: "https://static.wikia.nocookie.net/lotr/images/1/13/King_Theoden_1.jpg/revision/latest?cb=20150313135224");

            migrationBuilder.UpdateData(
                table: "Character",
                keyColumn: "Id",
                keyValue: 4,
                column: "PictureURL",
                value: "https://static.wikia.nocookie.net/lotr/images/e/e1/Gollum_Render.png/revision/latest?cb=20141218075509");

            migrationBuilder.UpdateData(
                table: "Character",
                keyColumn: "Id",
                keyValue: 5,
                column: "PictureURL",
                value: "https://static.wikia.nocookie.net/lotr/images/0/0a/Pippinprintscreen.jpg/revision/latest?cb=20060310083048");

            migrationBuilder.UpdateData(
                table: "Character",
                keyColumn: "Id",
                keyValue: 6,
                column: "PictureURL",
                value: "https://static.wikia.nocookie.net/lotr/images/6/64/Arwen_-_The_Fellowship_Of_The_Ring.jpg/revision/latest?cb=20210625164207");

            migrationBuilder.UpdateData(
                table: "Character",
                keyColumn: "Id",
                keyValue: 7,
                column: "PictureURL",
                value: "https://static.wikia.nocookie.net/disney/images/6/66/Profile_-_Scar.jpeg/revision/latest/scale-to-width-down/1032?cb=20190312021344");

            migrationBuilder.UpdateData(
                table: "Movie",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "MoviePosterURL", "TrailerURL" },
                values: new object[] { "https://www.themoviedb.org/t/p/original/6oom5QYQ2yQTMJIbnvbkBL9cHo6.jpg", "https://www.youtube.com/watch?v=_nZdmwHrcnw" });

            migrationBuilder.UpdateData(
                table: "Movie",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "MoviePosterURL", "TrailerURL" },
                values: new object[] { "https://www.themoviedb.org/t/p/original/5VTN0pR8gcqV3EPUHHfMGnJYN9L.jpg", "https://www.youtube.com/watch?v=nuTU5XcZTLA" });

            migrationBuilder.UpdateData(
                table: "Movie",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "MoviePosterURL", "TrailerURL" },
                values: new object[] { "https://images.theposterdb.com/prod/public/images/posters/optimized/movies/3280/03UBQg5JuDTIXa64mbQy6wfhIL27F9l45AM5KRGl.webp", "https://www.youtube.com/watch?v=lFzVJEksoDY" });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Character",
                keyColumn: "Id",
                keyValue: 1,
                column: "PictureURL",
                value: "https://static.wikia.nocookie.net/middle-earth-film-saga/images/7/76/Merry_the_two_towers.png/revision/latest?cb=20160207043604");

            migrationBuilder.UpdateData(
                table: "Character",
                keyColumn: "Id",
                keyValue: 2,
                column: "PictureURL",
                value: "Pic");

            migrationBuilder.UpdateData(
                table: "Character",
                keyColumn: "Id",
                keyValue: 3,
                column: "PictureURL",
                value: "Pic");

            migrationBuilder.UpdateData(
                table: "Character",
                keyColumn: "Id",
                keyValue: 4,
                column: "PictureURL",
                value: "Pic");

            migrationBuilder.UpdateData(
                table: "Character",
                keyColumn: "Id",
                keyValue: 5,
                column: "PictureURL",
                value: "Pic");

            migrationBuilder.UpdateData(
                table: "Character",
                keyColumn: "Id",
                keyValue: 6,
                column: "PictureURL",
                value: "Pic");

            migrationBuilder.UpdateData(
                table: "Character",
                keyColumn: "Id",
                keyValue: 7,
                column: "PictureURL",
                value: "Pic");

            migrationBuilder.UpdateData(
                table: "Movie",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "MoviePosterURL", "TrailerURL" },
                values: new object[] { "poster", "trailer" });

            migrationBuilder.UpdateData(
                table: "Movie",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "MoviePosterURL", "TrailerURL" },
                values: new object[] { "poster", "trailer" });

            migrationBuilder.UpdateData(
                table: "Movie",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "MoviePosterURL", "TrailerURL" },
                values: new object[] { "poster", "trailer" });
        }
    }
}
