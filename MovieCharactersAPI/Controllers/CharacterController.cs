﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Models.DTO.Character;
using MovieCharactersAPI.Models.DTO.Franchise;
using WebAPIWithEntityFramework.Services.Characters;

namespace MovieCharactersAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class CharacterController : ControllerBase
    {
        private readonly ICharacterService _characterService;
        private readonly IMapper _mapper;

        public CharacterController(ICharacterService characterService, IMapper mapper)
        {
            _characterService = characterService;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all Characters
        /// </summary>
        /// <returns></returns>
        // GET: api/Character
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterDTO>>> GetCharacters()
        {
            return Ok( _mapper.Map<List<CharacterDTO>>(await _characterService.GetAllAsync()));
        }

        /// <summary>
        /// Get a Character based on Id input
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/Character/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterDTO>> GetCharacter(int id)
        {
            var character = await _characterService.GetByIdAsync(id);

            if (character == null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<CharacterDTO>(character));
        }

        /// <summary>
        /// Updates the basic variables of a Character
        /// </summary>
        /// <param name="id"></param>
        /// <param name="character"></param>
        /// <returns></returns>
        // PUT: api/Character/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, CharacterPutDTO character)
        {
            if (id != character.Id)
            {
                return BadRequest();
            }
            await _characterService.UpdateAsync(_mapper.Map<Character>(character));

            return NoContent();
        }

        /// <summary>
        /// Deletes a franchise based on Id provided
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _characterService.Delete(id); ;
                return NoContent();
        }

        /// <summary>
        /// Posts a new Character with basic variables.
        /// </summary>
        /// <param name="characterDTO"></param>
        /// <returns></returns>
        // POST: api/Character
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult> PostCharacter(CharacterPostDTO characterDTO)
        {
            Character character = _mapper.Map<Character>(characterDTO);
            await _characterService.AddAsync(character);

            return CreatedAtAction("GetCharacter", new { id = character.Id }, character);
        }
        
    }
}
