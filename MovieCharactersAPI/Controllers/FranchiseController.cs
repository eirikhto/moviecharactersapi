﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Models.DTO.Character;
using MovieCharactersAPI.Models.DTO.Franchise;
using MovieCharactersAPI.Models.DTO.Movie;
using WebAPIWithEntityFramework.Services.Franchises;


namespace MovieCharactersAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class FranchiseController : ControllerBase
    {

        private readonly IFranchiseService _franchiseService;
        private readonly IMapper _mapper;

        public FranchiseController(IFranchiseService franchiseService, IMapper mapper)
        {
            _franchiseService = franchiseService;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all Franchises
        /// </summary>
        /// <returns></returns>
        // GET: api/Franchise
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseDTO>>> GetFranchises()
        {
            return Ok(_mapper.Map<List<FranchiseDTO>>(await _franchiseService.GetAllAsync()));
        }
        
        /// <summary>
        /// Get a franchise based on Id input
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/Franchise/5
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseDTO>> GetFranchise(int id)
        {
            var franchise =  _mapper.Map<FranchiseDTO>( await _franchiseService.GetByIdAsync(id));

            if (franchise == null)
            {
                return NotFound();
            }

            return franchise;
        }


        /// <summary>
        /// Gets all the movies of the franchise specified by the id parameter
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}/movies")]
        public async Task<ActionResult<IEnumerable<MovieDTO>>> GetAllMoviesInFranchise(int id)
        {
            var movies = _mapper.Map<List<MovieDTO>>(await _franchiseService.GetAllMoviesInFranchiseAsync(id));

            if (movies == null)
            {
                return NotFound();
            }

            return Ok(movies);
        }

        /// <summary>
        /// Gets all the characters of the franchise specified by the id parameter
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<IEnumerable<CharacterDTO>>> GetAllCharactersInFranchise(int id)
        {
            var characters = _mapper.Map<List<CharacterDTO>>(await _franchiseService.GetAllCharactersInFranchiseAsync(id));

            if (characters == null)
            {
                return NotFound();
            }

            return Ok(characters);
        }

        /// <summary>
        /// Updates the basic variables of a franchise
        /// </summary>
        /// <param name="id"></param>
        /// <param name="franchise"></param>
        /// <returns></returns>
        // PUT: api/Franchise/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, FranchisePutDTO franchise)
        {
            if (id != franchise.Id)
            {
                return BadRequest();
            }
            await _franchiseService.UpdateAsync(_mapper.Map<Franchise>(franchise));
            
            return NoContent();
        }

        /// <summary>
        /// Updates the foreign key 'Movies' of a selected franchise Id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movies"></param>
        /// <returns></returns>
        [HttpPut("{id}/movies")]
        public async Task<IActionResult> UpdateMoviesInFranchise(int id, int[] movies)
        {
            await _franchiseService.UpdateMoviesInFranchise(id, movies);

            return NoContent();
        }

        /// <summary>
        /// Deletes a franchise based on Id provided
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _franchiseService.Delete(id); ;
            return NoContent();
        }

        /// <summary>
        /// Posts a new franchise with basic variables.
        /// </summary>
        /// <param name="franchiseDTO"></param>
        /// <returns></returns>
        // POST: api/Franchise
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult> PostFranchise(FranchisePostDTO franchiseDTO)
        {
            Franchise franchise = _mapper.Map<Franchise>(franchiseDTO);
            await _franchiseService.AddAsync(franchise);

            return CreatedAtAction("GetFranchise", new { id = franchise.Id }, franchise);
        }
    }
}
