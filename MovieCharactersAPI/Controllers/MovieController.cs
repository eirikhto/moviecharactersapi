﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Models.DTO.Character;
using MovieCharactersAPI.Models.DTO.Movie;
using WebAPIWithEntityFramework.Services.Movies;

namespace MovieCharactersAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class MovieController : ControllerBase
    {
        private readonly IMovieService _movieService;
        private readonly IMapper _mapper;

        public MovieController(IMovieService movieService, IMapper mapper)
        {
            _movieService = movieService;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all Franchises
        /// </summary>
        /// <returns></returns>
        // GET: api/Movie
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieDTO>>> GetMovies()
        {
            return Ok( _mapper.Map<List<MovieDTO>>(await _movieService.GetAllAsync()));
        }

        /// <summary>
        /// Get a movie based on Id input
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/Movie/5
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieDTO>> GetMovie(int id)
        {
            var movie = _mapper.Map<MovieDTO>(await _movieService.GetByIdAsync(id));

            if (movie == null)
            {
                return NotFound();
            }

            return movie;
        }


        /// <summary>
        /// Gets all characters of the movie specified by the id parameter
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<IEnumerable<CharacterDTO>>> GetCharactersInMovie(int id)
        {
            var characters = _mapper.Map<List<CharacterDTO>>(await _movieService.GetAllCharactersInMovieAsync(id));

            if (characters == null)
            {
                return NotFound();
            }

            return Ok(characters);
        }

        /// <summary>
        /// Updates the basic variables of a movie
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movie"></param>
        /// <returns></returns>
        // PUT: api/Movie/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, MoviePutDTO movie)
        {
            if (id != movie.Id)
            {
                return BadRequest();
            }
            await _movieService.UpdateAsync(_mapper.Map<Movie>(movie));

            return NoContent();
        }

        /// <summary>
        /// Updates the foreign key 'Character' of a selected movie Id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="characters"></param>
        /// <returns></returns>
        [HttpPut("{id}/characters")]
        public async Task<IActionResult> UpdateCharactersInMovie(int id, int[] characters)
        {

            await _movieService.UpdateCharactersInMovie(id, characters);

            return NoContent();
        }

        /// <summary>
        /// Deletes a Movie based on Id provided
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _movieService.Delete(id); ;
            return NoContent();
        }

        /// <summary>
        /// Posts a new Movie with basic variables.
        /// </summary>
        /// <param name="movieDTO"></param>
        /// <returns></returns>
        // POST: api/Movie
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult> PostMovie(MoviePostDTO movieDTO)
        {
            Movie movie = _mapper.Map<Movie>(movieDTO);
            await _movieService.AddAsync(movie);
            return CreatedAtAction("GetMovie", new { id = movie.Id }, movie);
        }
    }
}
