﻿using AutoMapper;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Models.DTO.Character;

namespace MovieCharactersAPI.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile() 
        {  
            CreateMap<Character, CharacterDTO>()
            .ForMember(dto => dto.Movies, opt => opt.MapFrom(c => c.Movies.Select(m => m.Id).ToList()));

            CreateMap<CharacterPostDTO, Character>();
            
            CreateMap<CharacterPutDTO, Character>();
        }
    }
}
