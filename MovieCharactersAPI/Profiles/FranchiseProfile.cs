﻿using MovieCharactersAPI.Models.DTO.Character;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Models.DTO.Franchise;
using AutoMapper;

namespace MovieCharactersAPI.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseDTO>()
            .ForMember(dto => dto.Movies, opt => opt.MapFrom(f => f.Movies.Select(m => m.Id).ToList()));

            CreateMap<FranchisePostDTO, Franchise>();

            CreateMap<FranchisePutDTO, Franchise>();
        }
    }
}
