﻿using MovieCharactersAPI.Models;
using AutoMapper;
using MovieCharactersAPI.Models.DTO.Movie;

namespace MovieCharactersAPI.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, MovieDTO>()
            .ForMember(dto => dto.Franchise, opt => opt
            .MapFrom(m => m.FranchiseId))
            .ForMember(dto => dto.Characters, opt => opt
            .MapFrom(m => m.Characters.Select(c => c.Id).ToList()));

            CreateMap<MoviePostDTO, Movie>(); 

            CreateMap<MoviePutDTO, Movie>();


        }
    }
}
