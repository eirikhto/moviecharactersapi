# MovieCharactersAPI 

The purpose of this project is to use C# to create a database with EF Core and create a web API

## Name
Movie Character API

## Description
This project sets up a database with the use of EF Core. It sets up data attributes and relationships. We also create dummy data using seeded data.

The API is set up with a generic CRUD. It also has the ability to update related data. The project has DTO's with automapper. DTO's ecapulate all requests.
We also have swagger documentation.

### Dependencies

* Program able to open and run C# project

* Connection string to build a database

## Installation

*You can download the poject here (https://gitlab.com/eirikhto/moviecharactersapi)

Open in VS2022, change connection string from appsettings.json. Then in package manager console and run:

update-database

## Authors and acknowledgment

Eirik Torp  (https://gitlab.com/eirikhto)

Jonas Bjorhei (https://github.com/JonasBjorhei)

